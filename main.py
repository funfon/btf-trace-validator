import sys
import logging

from btf_trace_validator import BTFTraceValidator

logger = logging.getLogger(__name__)


def main():
    if __name__ == '__main__':

        try:
            trace_path = sys.argv[1]

        except IndexError as ie:
            logger.error(f'Failed to get btf trace path: {ie}')
            trace_path = None

        try:
            error_path = sys.argv[2]

        except IndexError as ie:
            error_path = 'error.txt'

        btfv = BTFTraceValidator(btf_trace_path=trace_path, btf_error_path=error_path)
        btfv.validate()


main()
