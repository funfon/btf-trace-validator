import logging

import pandas as pd

logger = logging.getLogger(__name__)


class BTFTraceValidator:
    """
    Class that validating for wrong events in btf trace file. If any found it will be written into a text file

    Parameters
    ----------
    btf_trace_path : str
        String that contains path to btf trace file that should be validated

    btf_error_path: str
        String that contains path to txt file where errors from btf trace file should be written

    Examples
    --------
    Constructing BTFTraceValidator.

    >>> btp = 'btf_trace.btf'
    >>> bep = 'btf_errors.txt'
    >>> btv = BTFTraceValidator(btf_trace_path=btp, btf_error_path=bep)
    >>> btv.btf_trace_path
        btf_trace.btf
    >>> btv.btf_error_path
        btf_errors.txt

    """

    def __init__(self, btf_trace_path=None, btf_error_path=None):
        if btf_trace_path is None:
            self.btf_trace_path = ''
        else:
            self.btf_trace_path = btf_trace_path

        if btf_error_path is None:
            self.btf_error_path = ''
        else:
            self.btf_error_path = btf_error_path

        self.trc = {}
        self.error_list = []
        self.start_event = 'activate'
        self.prior_events = {
            'activate': 1,
            'start': 2,
            'preempt': 3,
            'resume': 4,
            'terminate': 5,
        }

    def validate(self):
        """
        Validating btf trace file, if any error found append it into a text file
        """

        try:
            df = pd.DataFrame(self._retrieve_data(), columns=['TS', 'PE', 'SIN', 'Type', 'Element', 'TIN', 'Event'])

            self._find_errors(df)

            if self.error_list:
                self._write_errors()

        except ValueError as ve:
            logger.error(ve)

    def _retrieve_data(self):
        """
        Retrieving data from btf trace file

        :return: list of lists for each line in btf trace file
        """
        try:
            with open(self.btf_trace_path, 'r') as file:
                return [x.replace('\n', '').split(',') for x in file.readlines()[1:]]
        except FileNotFoundError as fnfe:
            logger.error(fnfe)

    def _find_errors(self, df):
        """
        Finding errors in btf trace file based on data frame

        :param df: pandas.DataFrame
        """
        for idx, row in df.iterrows():
            if row.get('PE', ''):
                self._validate_row(row, idx + 1)

    def _validate_row(self, row, line):
        """
        Validating row from btf trace file

        :param row: row from btf trace file
        """
        core = row.get('PE')

        if core in self.trc:
            elements = self.trc.get(core, {})

            self._validate_element(elements, row, line)

        else:
            if row.get('Event') != self.start_event:
                self._append_error_list(row.get('TS'), line, 'Wrong order of events from same task')

            else:
                self._append_row(row)

    def _validate_element(self, elements, row, line):
        """
        Validating elements, checks for errors
        """
        el_event_number = self._get_highest_event_number(elements)
        self._append_element(elements, row, line)

        for idx, el in enumerate(elements):
            r_number = self._get_event_number(row.get('Event'))
            c_number = self._get_event_number(el.get('Event'))
            error = False

            if el.get('Element') == row.get('Element') and el.get('TIN') == row.get('TIN'):

                if el_event_number > 1 and r_number == 2 and len(elements) > 1:
                    self._append_error_list(row.get('TS'), line, 'Two task active at same time range')
                    elements.pop(idx)

                if r_number - 1 != c_number and r_number > 1:
                    error = True

                if r_number == self.prior_events.get('terminate') and c_number == self.prior_events.get('start'):
                    error = False
                    el['Event'] = row.get('Event')

                else:
                    el['Event'] = row.get('Event')

                if error:
                    self._append_error_list(row.get('TS'), line, 'Wrong order of events from same task')

            if el.get('Event') == 'terminate':
                elements.pop(idx)

    def _get_highest_event_number(self, elements):
        """
        Get most priority element in core
        """
        el_event_number = 0

        for el in elements:
            if self._get_event_number(el.get('Event')) > el_event_number:
                el_event_number = self._get_event_number(el.get('Event'))

        return el_event_number

    def _get_event_number(self, event):
        """
        :return: event number by priority
        """
        number = 0
        if event == 'activate':
            number = 1
        elif event == 'start':
            number = 2
        elif event == 'preempt':
            number = 3
        elif event == 'resume':
            number = 4
        elif event == 'terminate':
            number = 5

        return number

    def _append_element(self, elements, row, line):
        """
        Appending element to row if it's new
        """
        c = 0

        for el in elements:
            if el.get('TIN') != row.get('TIN') and el.get('Element') != row.get('Element'):
                c += 1

        if c == len(elements):
            if self.start_event == row.get('Event'):
                elements.append({'Element': row.get('Element'), 'TIN': row.get('TIN'), 'Event': row.get('Event')})
            else:
                self._append_error_list(row.get('TS'), line, 'Wrong order of events from same task')

    def _append_error_list(self, ts, line, msg):
        """
        Appending error to error list

        :param ts: timestamp
        :param line: line with error in btf trace file
        :param msg: error message
        """
        self.error_list.append(f'{ts},{line},{msg}')

    def _append_row(self, row):
        """
        Appending row to current trace validator

        Examples:
            {'Core1': [{'Element': 'T1', 'TIN': 272, 'Event': 'activate'}]}
        """
        self.trc[row.get('PE')] = [{
            'Element': row.get('Element'),
            'TIN': row.get('TIN'),
            'Event': row.get('Event')
        }]

    def _write_errors(self):
        """
        Writing errors from error list to file
        """
        with open(self.btf_error_path, 'w') as f:
            for error in self.error_list:
                f.write(f'{error}\n')
