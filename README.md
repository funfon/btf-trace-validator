# BTF Trace validator

Class that validates given BTF trace and write all errors to given file.txt

### Prerequisites

```
python 3.9
```

### Installing

 - Create virtual environment
 - Install using Makefile
 
 ```shell script
$ make install
```

 - Run project using command, first argument must be btf_trace_file, second argument must be error.txt file:
 ```shell script
$ python3 main.py <btf_trace_file.btf> <error_file.txt>
```
   btf trace file path is required in arguments while error txt file will be replace by 'error.txt' by default.

## Authors
* **Ilia Levikov** - *Initial work* - [funfon](https://gitlab.com/funfon/)